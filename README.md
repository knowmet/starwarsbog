# StarWarsBog

Repositorio destinado al proyecto Android Star Wars para Opinno desarrollado por Bogdan.

--Librerias utilizadas --

Se han utilizado las siguientes librerías:

*AndroidImageSlider : Se ha utilizado esta librería ya que permite realizar un carousel de imagenes de forma facil.
	Link: https://github.com/daimajia/AndroidImageSlider
	Dependencias:     	implementation "com.android.support:support-v4:+"
						implementation 'com.squareup.picasso:picasso:2.3.2'
						implementation 'com.nineoldandroids:library:2.4.0'
						implementation 'com.daimajia.slider:library:1.1.5@aar'
						
*Cardview Y recycleView: Se han utilizado estas librerías para poder realizar unos cardview con las películas a mostrar.
	Dependencias: 		implementation 'com.android.support:cardview-v7:27.1.+'
						implementation 'com.android.support:recyclerview-v7:27.1.+'
						
*Volley: Se ha utilizado Volley para realizar las llamadas HTTP a la api proporcionada de forma comoda.
	Dependencias:		implementation 'com.android.volley:volley:1.1.1'
	
*Picasso: Se utiliza picasso para tratar de forma facil las imagenes de las caratulas en los detalles. Se implementa con las dependencias
de AndroidImageSlider por lo que opte por utilizar la misma versión (aunque antigua pero funcional).

--Comentarios adicionales--

- El historial muestra un máximo de 5 pestañas visitadas anteriormente y es persistente, no se borra al cerrar la app.
- La API solo proporcionaba información sobre 7 peliculas.
- La API no contenia ninguna imagen por lo que las caratulas de las 7 películas son almacenadas dentro de la app y no bajadas desde la api.
