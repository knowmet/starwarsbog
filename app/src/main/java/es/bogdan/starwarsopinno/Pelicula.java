package es.bogdan.starwarsopinno;

import java.io.Serializable;
import java.util.ArrayList;

//Clase para generar instancias para cada pelicula y poder pasarla de forma adecuada al adaptador.
//Usaremos Serializable para poder mandar el objeto a traves de activitys de forma sencilla.
public class Pelicula implements Serializable{
    private int caratula;
    private String nombre;
    private String director;
    private String productor;
    private String estreno;
    private String link;

    Pelicula(int caratula, String nombre, String director, String productor, String estreno, String link){
        this.caratula=caratula;
        this.nombre=nombre;
        this.director=director;
        this.productor=productor;
        this.estreno=estreno;
        this.link=link;
    }
    public int getCaratula() {
        return caratula;
    }

    public void setCaratula(int caratula) {
        this.caratula = caratula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getEstreno() {
        return estreno;
    }

    public void setEstreno(String estreno) {
        this.estreno = estreno;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }



}
