package es.bogdan.starwarsopinno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetallesPeliculas extends AppCompatActivity {

    private Pelicula pelicula;
    private CustomDialogProgress dialogProgress;
    private ImageView caratula;
    private TextView titulo;
    private TextView director;
    private TextView productor;
    private TextView estreno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_peliculas);
        dialogProgress = new CustomDialogProgress(this);
        dialogProgress.setCancelable(false);
        dialogProgress.show();
        //Linkeamos todas las variables
        caratula = findViewById(R.id.poster_detallesPeliculas);
        titulo = findViewById(R.id.titulo_detallesPeliculas);
        director = findViewById(R.id.director_detallesPeliculas);
        productor = findViewById(R.id.productor_detallesPeliculas);
        estreno = findViewById(R.id.estreno_detallesPeliculas);

        //Recuperamos el intent mediante el cual hemos llegado a este activity para sacarle el objeto Pelicula.
        Intent intent = getIntent();
        pelicula = (Pelicula) intent.getSerializableExtra("objeto_pelicula");
        //Utilizamos Picasso para cargar la foto dentro del imageView
        Picasso.with(this).load(pelicula.getCaratula()).into(caratula);
        //Cargamos los datos del resto de variables
        titulo.setText(pelicula.getNombre());
        director.setText(pelicula.getDirector());
        productor.setText(pelicula.getProductor());
        estreno.setText(pelicula.getEstreno());
        //Una vez tengamos todos los datos cargados quitamos el dialogProgress
        dialogProgress.dismiss();

    }
}
