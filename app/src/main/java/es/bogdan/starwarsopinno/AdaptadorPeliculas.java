package es.bogdan.starwarsopinno;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdaptadorPeliculas extends RecyclerView.Adapter<AdaptadorPeliculas.PeliculaViewHolder> {

    private ArrayList<Pelicula>peliculasArrayList;
    private Context context;
    //Variables necesarias para el sharedPreferences
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final int PREFERENCE_MODE_PRIVATE=0;
    private static final String PREFERENCE_FILE="Historial";

    AdaptadorPeliculas(ArrayList<Pelicula> peliculasArrayList, Context context){
        this.peliculasArrayList=peliculasArrayList;
        this.context=context;
        //Configuramos el sharedPreference
        sharedPreferences = context.getSharedPreferences(PREFERENCE_FILE,PREFERENCE_MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }

    public class PeliculaViewHolder extends RecyclerView.ViewHolder{

        CardView cv;
        ImageView caratula;
        TextView nombre;

        public PeliculaViewHolder(View itemView) {
            super(itemView);
            //Linkeamos todos los componentes del cardview con nuestros atribudos del adaptador.
            cv = itemView.findViewById(R.id.cv);
            caratula = itemView.findViewById(R.id.caratula_pelicula);
            nombre = itemView.findViewById(R.id.txt_titulo_pelicula);

        }
    }

    //La inicialización del viewHolder en el que indicamos que usaremos el cardview cada vez
    @NonNull
    @Override
    public PeliculaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        PeliculaViewHolder pvh = new PeliculaViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final PeliculaViewHolder holder, final int position) {
            holder.caratula.setImageResource(peliculasArrayList.get(position).getCaratula());
            holder.caratula.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            //Añadimos un onClickListener a la caratula para poder abrir la pelicula buscada mediante un Intent
            holder.caratula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Guardamos la pelicula seleccionada en el historial
                editor.putString("historial",setHistorial(sharedPreferences.getString("historial","default"),peliculasArrayList.get(position).getLink()));
                editor.commit();
                //Dependiendo de que pelicula se ha seleccionado la abrimos en el activity DetallesPeliculas
                Intent intentDetallesPeliculas = new Intent(context,DetallesPeliculas.class);
                intentDetallesPeliculas.putExtra("objeto_pelicula",peliculasArrayList.get(position));
                context.startActivity(intentDetallesPeliculas);

            }
        });
            holder.nombre.setText(peliculasArrayList.get(position).getNombre());
    }

    @Override
    public int getItemCount() {
        return peliculasArrayList.size();
    }

    //Metodo para introducir los nuevos datos al adaptador con cada busqueda.
    public void setNewData(ArrayList<Pelicula> peliculasArrayList){
        this.peliculasArrayList=peliculasArrayList;
        this.notifyDataSetChanged();
    }


    //Metodo para añadir la nueva busqueda a nuestro sharedPreferences y eliminar la ultima (de ser necesario), mostraremos un maximo de 5 busquedas.
    public String setHistorial(String oldHistorial, String search){
        String newHistorial="";
        //Comprobamos que exista historial ya, si no, devolvemos la primera busqueda y ya
        if(!oldHistorial.equals("default")) {
            //Usamos un arrayList Auxiliar para poder tratar mas facilmente el mover elementos y colocar la busqueda mas reciente en primera posición
            //ademas de borrar la ultima.
            ArrayList<String> aux = new ArrayList<>();
            //Separamos el string de busquedas mediante el separador -
            String[] individualSearch = oldHistorial.split("-");
            for (int i = 0; i < individualSearch.length; i++) {
                //Añadimos todas las busquedas separadas al arrayList auxiliar
                aux.add(individualSearch[i]+"-");
            }
            //Colocamos la busqueda mas reciente en primera posición y le añadimos el separador
            aux.add(0, search + "-");
            //Eliminamos la busqueda mas antigua.
            if (aux.size() > 5) {
                aux.remove(aux.size() - 1);
            }
            //Volvemos a unir el historial en un solo String
            for (int b = 0; b < aux.size(); b++) {
                newHistorial = newHistorial + aux.get(b);
            }
            return newHistorial;
        }else{
            return search+"-";
        }
    }
}
