package es.bogdan.starwarsopinno;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private SliderLayout sliderCarousel;
    private RecyclerView rv;
    private AdaptadorPeliculas adaptador;
    private CustomDialogProgress dialogProgress;
    private RequestQueue mRequestQueue;
    private ArrayList<Pelicula> arrayListPeliculas;
    private AutoCompleteTextView searchBox;
    //Variables necesarias para el sharedPreferences
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final int PREFERENCE_MODE_PRIVATE=0;
    private static final String PREFERENCE_FILE="Historial";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sliderCarousel = findViewById(R.id.slider_carousel);
        searchBox = findViewById(R.id.autocomplete_buscador);
        //Configuramos el custom dialog para no poder ser cancelado y pararlo solo cuando estimemos oportuno
        dialogProgress = new CustomDialogProgress(this);
        dialogProgress.setCancelable(false);
        //Configuramos el sharedPreference
        sharedPreferences = getSharedPreferences(PREFERENCE_FILE,PREFERENCE_MODE_PRIVATE);
        editor=sharedPreferences.edit();
        //Configuramos el requestQueue para poder hacer peticiones HTTP a la api mediante vollei
        // Instaciamos la cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        //Configuramos la HTTP Network
        Network network = new BasicNetwork(new HurlStack());
        // Instanciamos la requesQueue con el objeto cache y network configurados anteriormente.
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        //Configuramos el searchBox
        String[] nombres_peliculas_array = getResources().getStringArray(R.array.nombres_peliculas_array);
        ArrayAdapter<String> array_adapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,nombres_peliculas_array);
        searchBox.setAdapter(array_adapter);

        //Comenzamos la configuracion del recycleView
        rv = findViewById(R.id.recycleView);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(llm);
        //Instanciamos el adaptador y le pasamos el array de peliculas y el context de esta activity para poder realizar intents
        arrayListPeliculas = new ArrayList<>();
        adaptador = new AdaptadorPeliculas(arrayListPeliculas,this);
        //Le linkeamos el adaptador al recivle view
        rv.setAdapter(adaptador);
        //HashMap para recoger la relacion de las imagenes y sus titulos y posteriormente pasarlos de forma ordenada al TextSliderView
        HashMap<String,Integer> character_pictures_map = new HashMap<String, Integer>();
        character_pictures_map.put("R2D2 and C3PO",R.drawable.androides);
        character_pictures_map.put("Kylo Ren",R.drawable.batamantero1);
        character_pictures_map.put("Chewbacca",R.drawable.chewbacca);
        character_pictures_map.put("Darth Vader",R.drawable.darthvader);
        character_pictures_map.put("Yoda",R.drawable.enanoverde);
        character_pictures_map.put("Stormtrooper",R.drawable.stormtrooper);

        for (String name: character_pictures_map.keySet()){

            //Creamos y inicializamos cada textSliderView que almacena una imagen y su descripcion
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .description(name)
                    .image(character_pictures_map.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside);
            //Se añade el textSliderView al carousel
            sliderCarousel.addSlider(textSliderView);
        }

        //Configuracion adicional del Slider
            //Animacion del slider
        sliderCarousel.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
            //Duracion de la imagen antes de cambiar - 3 segundos
        sliderCarousel.setDuration(3000);
        //Fin configuracion basica del slider

        sliderCarousel.startAutoCycle();
    }

    @Override
    protected void onStop() {
        //Como aconseja el autor de la libraria, debemos parar la rotacion cuando se pare el activity
        //para eviar bloqueos debido a la memoria
        sliderCarousel.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onStart() {
        //Tras un onStop queremos que el autoCyle vuelva a funcionar.
        sliderCarousel.startAutoCycle();
        super.onStart();
    }

    //Metodo que es llamado al pulsar el botón buscar
    public void btnBuscar(View v){
        dialogProgress.show();
        getPeliculas(searchBox.getText().toString());
    }

    //Metodo para hacer petición a la API de bajar el JSON que nos proporciona
    public void getPeliculas(String titulo){
        //Inicializamos el arrayList que guardara las peliculas que mostraremos tras la busqueda
        arrayListPeliculas=new ArrayList<>();
        String url = "https://swapi.co/api/films/?search="+titulo+"&format=json";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Guardamos en un array cada una de las respuestas de las peliculas para tratarlas una a una.
                            JSONArray arrayPeliculas=response.getJSONArray("results");
                            for (int i=0;i<arrayPeliculas.length();i++){
                                JSONObject pelicula = arrayPeliculas.getJSONObject(i);
                                arrayListPeliculas.add(new Pelicula(asignCaratulas(pelicula.getString("title")),pelicula.getString("title"),
                                        pelicula.getString("director"),pelicula.getString("producer"),pelicula.getString("release_date"),
                                        pelicula.getString("url")));
                            }
                            //Una vez tengamos todas las peliculas en el arrayListPeliculas, tendremos que notificar al adaptador de que los
                            //datos han cambiado.
                            adaptador.setNewData(arrayListPeliculas);
                            //Quitamos el mensaje de "cargando"
                            dialogProgress.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        dialogProgress.dismiss();
                        Toast toast = Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
        //Realizamos la peticion definida en el metodo a volley
        mRequestQueue.add(jsonObjectRequest);
    }

    //Metodo para que segun el nombre de la pelicula le podamos asignar su caratula correspondiente.
    public int asignCaratulas(String name){
        switch (name){
            case "A New Hope":
              return R.drawable.a_new_hope;
            case "Attack of the Clones":
                return R.drawable.attack_of_the_clones;
            case "The Phantom Menace":
                return R.drawable.the_phantom_menace;
            case "Revenge of the Sith":
                return R.drawable.revenge_of_the_sith;
            case "Return of the Jedi":
                return R.drawable.return_of_the_jedi;
            case "The Empire Strikes Back":
                return R.drawable.the_empire_strikes_back;
            case "The Force Awakens":
                return R.drawable.the_force_awakens;
            default:
                return 0;
        }
    }
    //Metodo llamado por el boton historial
    public void btn_historial(View v){
        if(!sharedPreferences.contains("historial")){
            Toast toast = Toast.makeText(getApplicationContext(),"No hay historial que mostrar.", Toast.LENGTH_LONG);
            toast.show();
        }else{
            getHistorial(sharedPreferences.getString("historial","default"));
        }
    }

    //Metodo con el que separamos el sharedPreferences en links individuales del historial
    public void getHistorial(String historial){
        arrayListPeliculas = new ArrayList<>();
        dialogProgress.show();
        String[] individualLinks = historial.split("-");
        for (int i = 0; i < individualLinks.length; i++) {
            //Indicamos a la descarga si es la ultima o no de la lista de peliculas
            if(i==individualLinks.length-1){
                descargarPeliculaHistorial(individualLinks[i],true);
            }else{
                descargarPeliculaHistorial(individualLinks[i],false);
            }
        }
    }

    //Si es la ultima película que descargamos, quitamos el cartel de "cargando..."
    public void descargarPeliculaHistorial(String link, final boolean ultima){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, link+"?format=json", null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            arrayListPeliculas.add(new Pelicula(asignCaratulas(response.getString("title")),response.getString("title"),
                                    response.getString("director"),response.getString("producer"),response.getString("release_date"),
                                    response.getString("url")));
                            if (ultima){
                                //Si es la ultima descarga quitamos el cartel de cargando.
                                dialogProgress.dismiss();
                                //Y mandamos los contenidos descargados al adaptador
                                adaptador.setNewData(arrayListPeliculas);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        //En caso de error quitamos el cartel.
                        dialogProgress.dismiss();
                    }
                });
        mRequestQueue.add(jsonObjectRequest);
    }

}
